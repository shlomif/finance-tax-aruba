use utf8;
package Finance::Tax::Aruba;
our $VERSION = '0.002';
use Moose;
use namespace::autoclean;

# ABSTRACT: A package that deal with tax calculations for Aruba

__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

This is a suite that deals with tax calculations for the island of Aruba.
Currently only income taxes for individuals are supported in their most basic
levels.

=head1 SEE ALSO

L<Finance::Tax::Aruba::Income>
